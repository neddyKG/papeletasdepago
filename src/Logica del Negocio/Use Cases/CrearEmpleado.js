let Empleado = require('../Entidades/Empleado/Empleado');
let TarjetaDeAsistencia = require('../Entidades/TarjetaDeAsistencia');
let MetodoDePagoFactory = require('../Entidades/Empleado/MetodoDePagoFactory');
let MediosDeNotificacionFactory = require('../Entidades/Empleado/MediosDeNotificacionFactory');

let SalarioFijo = require('../Entidades/Empleado/Salario Strategy/SalarioFijo');
let SalarioPorHora = require('../Entidades/Empleado/Salario Strategy/SalarioPorHora');
let SalarioComision = require('../Entidades/Empleado/Salario Strategy/SalarioComision');
let Venta = require('../Entidades/TarjetaDeVenta');
let Pertenece = require('../Entidades/Empleado/Sindicato Strategy/Pertenece');
let NoPertenece = require('../Entidades/Empleado/Sindicato Strategy/NoPertenece');

class CrearEmpleado{

crearEmpleadoFijo(datosEmpleadoFijo, repositorio){
    let asistencia = new TarjetaDeAsistencia(datosEmpleadoFijo.horaLlegada, datosEmpleadoFijo.horaSalida);
    asistencia.addAsistencia(datosEmpleadoFijo.horaSalida);
    datosEmpleadoFijo.asistencia = asistencia;
    let sindicato = this.crearSindicato(datosEmpleadoFijo);

    let salarioFijo = new SalarioFijo(datosEmpleadoFijo.salario, asistencia, sindicato);
    
    let metodoDePago = new MetodoDePagoFactory(datosEmpleadoFijo.metodoDePago); 
      
    let mediosDeNotificacionEmpleado = new MediosDeNotificacionFactory(datosEmpleadoFijo.mediosDeNotificacion, datosEmpleadoFijo.celular, datosEmpleadoFijo.email);
    

    let empleado = new Empleado(datosEmpleadoFijo.nombre, datosEmpleadoFijo.ci, null, salarioFijo, metodoDePago, mediosDeNotificacionEmpleado, datosEmpleadoFijo.celular, datosEmpleadoFijo.email, sindicato);
    repositorio.crearEmpleado(empleado);
 }

   

 crearEmpleadoComision(datosEmpleadoFijo, repositorio){
    let venta = new Venta();
    datosEmpleadoFijo.ventas.forEach(ventaRealizada => {
        venta.addVenta(ventaRealizada.fecha, ventaRealizada.monto);
    });

    let asistencia = new TarjetaDeAsistencia(datosEmpleadoFijo.horaLlegada, datosEmpleadoFijo.horaSalida);
    asistencia.addAsistencia(datosEmpleadoFijo.horaSalida);
    datosEmpleadoFijo.asistencia = asistencia;
    let sindicato = this.crearSindicato(datosEmpleadoFijo);
    
    let salarioComision = new SalarioComision(datosEmpleadoFijo.salario, datosEmpleadoFijo.comision, venta, asistencia, sindicato);
    
    let metodoDePago = new MetodoDePagoFactory(datosEmpleadoFijo.metodoDePago); 

    let mediosDeNotificacionEmpleado = new MediosDeNotificacionFactory(datosEmpleadoFijo.mediosDeNotificacion, datosEmpleadoFijo.celular, datosEmpleadoFijo.email);
    
    let empleado = new Empleado(datosEmpleadoFijo.nombre, datosEmpleadoFijo.ci, null, salarioComision, metodoDePago, mediosDeNotificacionEmpleado, datosEmpleadoFijo.celular, datosEmpleadoFijo.email, sindicato);
    repositorio.crearEmpleado(empleado);
 }


 
 crearEmpleadoHora(datosEmpleadoFijo, repositorio){
    let asistencia = new TarjetaDeAsistencia(datosEmpleadoFijo.horaLlegada, datosEmpleadoFijo.horaSalida);
    asistencia.addAsistencia(datosEmpleadoFijo.horaSalida);
    datosEmpleadoFijo.asistencia = asistencia;
    let sindicato = this.crearSindicato(datosEmpleadoFijo);
    let salarioHora = new SalarioPorHora(datosEmpleadoFijo.salario, asistencia, sindicato);
    
    let metodoDePago = new MetodoDePagoFactory(datosEmpleadoFijo.metodoDePago); 

    let mediosDeNotificacionEmpleado = new MediosDeNotificacionFactory(datosEmpleadoFijo.mediosDeNotificacion, datosEmpleadoFijo.celular, datosEmpleadoFijo.email);
    
    let empleado = new Empleado(datosEmpleadoFijo.nombre, datosEmpleadoFijo.ci, null, salarioHora, metodoDePago, mediosDeNotificacionEmpleado, datosEmpleadoFijo.celular, datosEmpleadoFijo.email, sindicato);
    repositorio.crearEmpleado(empleado);
 }
 
 crearSindicato(datosEmpleadoFijo) {
   let sindicato = null;
   if (datosEmpleadoFijo.sindicato === 'Pertenece')
      sindicato = new Pertenece();
   else
      sindicato = new NoPertenece();
   return sindicato;
}
    
}

module.exports = CrearEmpleado;