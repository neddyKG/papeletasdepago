let PapeletaDePago = require('../Entidades/Papeleta de Pago/PapeletaDePago');

class GenerarPapeletasDePago{

    constructor(empleados){
        this.empleados = empleados;
    }

    generarPapeletasDePago(){
        let papeletaDePago;
        let papeletas = [];
        this.empleados.forEach(empleado => {
            papeletaDePago = new PapeletaDePago(empleado);
            papeletas.push(papeletaDePago.generarPapeletaDePago());
        });
        return papeletas;
    } 

}

module.exports = GenerarPapeletasDePago;