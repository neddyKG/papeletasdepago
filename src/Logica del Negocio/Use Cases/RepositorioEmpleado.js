class RepositorioEmpleado{
    constructor(repository) {
        this.repository = repository;
      }
    
      crearEmpleado(empleado) {
        return this.repository.persistirEmpleado(empleado);
      }

      obtenerListaEmpleadosCorrespondePagar(fecha) {
        return this.repository.listaEmpleadosCorrespondePagar(fecha);
      }
    
      obtenerListaEmpleados() {
        return this.repository.listaEmpleados();
      }
    
}

module.exports = RepositorioEmpleado;
