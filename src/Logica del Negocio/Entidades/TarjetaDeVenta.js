class TarjetaDeVenta{

    constructor(){
        this.ventas = [];
    }

    addVenta(fecha, monto) {
        this.ventas.push({fecha: fecha, monto: monto});
    }

    getCantidadMonto() {
        let monto = 0;
        this.ventas.forEach(venta => {
            monto += venta.monto;
        });
        return monto;
    }

    getVentas() {
        return this.ventas;
    }
    
}

module.exports = TarjetaDeVenta;