
let EnCheque = require('../Empleado/Metodo de Pago Strategy/EnCheque');
let EnDeposito = require('../Empleado/Metodo de Pago Strategy/EnDeposito');
let EnEfectivo = require('../Empleado/Metodo de Pago Strategy/EnEfectivo');

class MetodoDePagoFactory{
    
    constructor(metodoDePagoDelEmpleado){
        this.metodoDePagoDelEmpleado = metodoDePagoDelEmpleado;
        return this.crearMetodoDePago();
    }

      crearMetodoDePago() {
        let metodoDePago = null;
        switch (this.metodoDePagoDelEmpleado) {
            case 'Pago en Cheque':
                metodoDePago = new EnCheque();
                break;
            case 'Pago en Deposito':
                metodoDePago = new EnDeposito();
                break;
            case 'Pago en Efectivo':
                metodoDePago = new EnEfectivo();
                break;
        }
        return metodoDePago;
    }
}

module.exports = MetodoDePagoFactory;
