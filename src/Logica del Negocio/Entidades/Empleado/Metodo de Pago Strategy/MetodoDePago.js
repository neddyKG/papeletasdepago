class MetodoDePago{

    constructor(tipoMetodoDePago){
        this.tipoMetodoDePago = tipoMetodoDePago;
    }

    elegirMetodoDePago(){
        return this.tipoMetodoDePago.elegirMetodoDePago();
    }

}

module.exports = MetodoDePago;