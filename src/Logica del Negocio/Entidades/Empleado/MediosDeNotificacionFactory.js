let MediosDeNotificacion = require('../Empleado/Medios de Notificacion Composite/MediosDeNotificacion');
let Email = require ('../Empleado/Medios de Notificacion Composite/Email');
let Facebook = require ('../Empleado/Medios de Notificacion Composite/Facebook');
let SMS = require ('../Empleado/Medios de Notificacion Composite/SMS');
let WhatsApp = require ('../Empleado/Medios de Notificacion Composite/WhatsApp');

class MediosDeNotificacionFactory{

    constructor(mediosDeNotificacion, celular, email){
        this.mediosDeNotificacion = mediosDeNotificacion;
        this.email = email;
        this.celular = celular;
        return this.crearMediosDeNotificacion();
    }

    crearMediosDeNotificacion() {
        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        console.log(this.mediosDeNotificacion);
        this.mediosDeNotificacion.forEach(medioDeNotificacion => {
            switch (medioDeNotificacion) {
                case 'Email':
                    let email = new Email(this.email);
                    mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(email);
                    break;
                case 'Facebook':
                    let facebook = new Facebook(this.email);
                    mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(facebook);
                    break;
                case 'SMS':
                    let sms = new SMS(this.celular);
                    mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(sms);
                    break;
                case 'Whatsapp':
                    let whatsapp = new WhatsApp(this.celular);
                    mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(whatsapp);
                    break;
            }
        });
        return mediosDeNotificacionEmpleado;
    }
    
}

module.exports = MediosDeNotificacionFactory;
