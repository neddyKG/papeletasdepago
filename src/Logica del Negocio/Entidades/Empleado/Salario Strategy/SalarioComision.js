class SalarioComision{

    constructor(salario, comision, venta, asistencia, sindicato){
        this.salario = salario; 
        this.comision = comision; 
        this.venta = venta;
        this.asistencia = asistencia;
        this.sindicato = sindicato;
    }
    
    calcularSalario(){
        if (this.sindicato.perteneceAlSindicato()) {
            return this.salario + this.getComision() - 150;
        } else {
            return this.salario + this.getComision();
        }
    }

    getComision() {
        return this.venta.getCantidadMonto() * this.comision;
    }

    getAsistencia() {
        return this.asistencia;
    }

    getVenta() {
        return this.venta;
    }

    getPorcentajeComision() {
        return this.comision;
    }

}

module.exports = SalarioComision;
