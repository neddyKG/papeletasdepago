class SalarioFijo{

    constructor(salario, asistencia, sindicato){
        this.salario = salario;
        this.asistencia = asistencia;
        this.sindicato = sindicato;
    }

    calcularSalario () {
        if (this.sindicato.perteneceAlSindicato()) {
            return this.salario - 100;
        } else {
            return this.salario;
        }
    }

    getAsistencia() {
        return this.asistencia;
    }

}

module.exports = SalarioFijo;