class CalculadoraSalario{

    constructor(tipoDeSalario){
        this.tipoDeSalario = tipoDeSalario;
    }

    calcularSalario(){
        return this.tipoDeSalario.calcularSalario();
    }

}

module.exports = CalculadoraSalario;
