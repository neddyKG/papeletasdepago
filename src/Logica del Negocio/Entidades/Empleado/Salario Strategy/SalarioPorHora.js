class SalarioPorHora{

    constructor(salario, asistencia, sindicato){
        this.salarioPorHora = salario; 
        this.asistencia = asistencia; 
        this.sindicato = sindicato;
    }
    
    calcularSalario(){
        if (this.sindicato.perteneceAlSindicato()) {
            return this.salarioPorHora * this.asistencia.getCantidadDeHoras() - 50;
        } else {
            return this.salarioPorHora * this.asistencia.getCantidadDeHoras();
        }
    }

    getAsistencia() {
        return this.asistencia;
    }

}

module.exports = SalarioPorHora;