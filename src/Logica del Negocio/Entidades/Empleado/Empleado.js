class Empleado {

    constructor (nombre, ci, calculadoraFechaDePago, calculadoraSalario, metodoPago, mediosDeNotificacion, celular, email, sindicato) {
        this.nombre = nombre;
        this.ci = ci;
        this.calculadoraSalario = calculadoraSalario;
        this.calculadoraFechaDePago = calculadoraFechaDePago;
        this.celular = celular;
        this.email = email;
        this.metodoPago = metodoPago;
        this.mediosDeNotificacion = mediosDeNotificacion;
        this.sindicato = sindicato;
    }

    calcularSalario(){
        return this.calculadoraSalario.calcularSalario();
    }

    correspondePagar(){
        return this.calculadoraFechaDePago.correspondePagar();
    }

    obtenerPago (){
        return this.metodoPago.elegirMetodoDePago();
    }

    perteneceAlSindicato (){
        return this.sindicato.perteneceAlSindicato();
    }
    correspondePagar() {
        return this.calculadoraFechaDePago.correspondePagar();
    }

    enviarNotificacion(){
    }

}

module.exports = Empleado;
