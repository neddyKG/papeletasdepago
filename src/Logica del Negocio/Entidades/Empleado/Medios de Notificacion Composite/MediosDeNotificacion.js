class MediosDeNotificacion{
    constructor(){
        this.mediosDeNotificacion = [];
    }

    agregarMedioDeNotificacion(medioDeNotificacion) {
        this.mediosDeNotificacion.push(medioDeNotificacion);
    }

    obtenerMediosDeNotificacionEmpleado() {
        let listaMediosDeNotificacion = [];
        this.mediosDeNotificacion.forEach(medioDeNotificacion => {
            listaMediosDeNotificacion.push(medioDeNotificacion.tipo);
        });
        return listaMediosDeNotificacion;
    }

    enviarNotificacion(papeletaDePago){
        let listaMediosDeNotificacion = '';
        this.mediosDeNotificacion.forEach(medioDeNotificacion => {
            listaMediosDeNotificacion += medioDeNotificacion.enviarNotificacion(papeletaDePago.generarPapeletaDePago()) + ' ';
        });
        return listaMediosDeNotificacion;
    }

    obtenerPapeletaDePago(papeletaDePago){
        return papeletaDePago.generarPapeletaDePago();
    }
}

module.exports = MediosDeNotificacion;