class ValidarDia{
    constructor(calculadoraSalario){
        this.calculadoraSalario = calculadoraSalario;
        this.asistencias = this.calculadoraSalario.asistencia.asistencias;
    }

    esViernes(){
        return this.getUltimaFechaDeAsistencia().getDay()=== 5;
    }

    noEsSabado(){
        return this.getUltimaFechaDeAsistencia().getDay()!== 6;
    }

    noEsDomingo(){
        return this.getUltimaFechaDeAsistencia().getDay()!== 0;
    }

    getUltimaFechaDeAsistencia() {
        return this.calculadoraSalario.asistencia.asistencias[this.asistencias.length - 1];
    }
}

module.exports = ValidarDia;
