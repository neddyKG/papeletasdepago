class CalculadoraFechaDePago{
    constructor(fechaDeTipoX){
        this.fechaDeTipoX = fechaDeTipoX;
    }

    correspondePagar(){
        return this.fechaDeTipoX.correspondePagar();
    }
}

module.exports = CalculadoraFechaDePago;