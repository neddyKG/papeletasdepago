let ValidarDia = require('./ValidarDia');
var moment = require('moment');
class FechaTipoFijo{
    constructor(calculadoraSalario){
        this.calculadoraSalario = calculadoraSalario;
        this.validarDia = new ValidarDia(this.calculadoraSalario);
    }
    
    correspondePagar(){
        if (this.esUltimoDiaHabilDelMes())
        return this.getFechaDePago().toDateString();

        return 'No corresponde pagarle!';
    }

    getFechaDePago() {
        return new Date(this.validarDia.getUltimaFechaDeAsistencia());
    }
    

    esUltimoDiaHabilDelMes(){
        return (this.esUltimoDiaDelMes() && this.validarDia.noEsDomingo() && this.validarDia.noEsSabado())
    }

    esUltimoDiaDelMes() {
        return this.getUltimoDiaDelMes(this.validarDia.getUltimaFechaDeAsistencia()) === moment(this.validarDia.getUltimaFechaDeAsistencia()).format("LL");
    }

    getUltimoDiaDelMes(date) {
        
        let ultimoDiaDelMes = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        return moment(ultimoDiaDelMes).format("LL");
    }
}

module.exports = FechaTipoFijo;
