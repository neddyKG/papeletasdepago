let ValidarDia = require('./ValidarDia');
var moment = require('moment');
class FechaPorHora{
    constructor(calculadoraSalario){
        this.calculadoraSalario = calculadoraSalario;
        this.validarDia = new ValidarDia(this.calculadoraSalario);
    }
    
    correspondePagar(){
        if (this.validarDia.esViernes())
        return this.getFechaDePago().toDateString();

        return 'No corresponde pagarle!';
    }

    getFechaDePago() {
        return new Date(this.validarDia.getUltimaFechaDeAsistencia());
    }
}

module.exports = FechaPorHora;
