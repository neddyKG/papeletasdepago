let ValidarDia = require('./ValidarDia');
var moment = require('moment');
class FechaTipoComision{
    constructor(calculadoraSalario){
        this.calculadoraSalario = calculadoraSalario;
        this.asistencias = this.calculadoraSalario.asistencia.asistencias;
        this.validarDia = new ValidarDia(this.calculadoraSalario);
    }
    
    correspondePagar(){
        if (this.validarDia.esViernes() && this.pasoDosSemanas())
        return this.getFechaDePago().toDateString();

        return 'No corresponde pagarle!';
    }

    getFechaDePago() {
        return new Date(this.validarDia.getUltimaFechaDeAsistencia())
    }
    
    pasoDosSemanas(){
        for (let asistencia = (this.asistencias.length)-2 ; asistencia >= 0; asistencia--) {
            if(this.trabajoLaAnteriorSemana(asistencia))
                return true;
        }
        return false;
    }

    trabajoLaAnteriorSemana(asistencia) {
        return this.getNumeroDeSemana(this.asistencias[asistencia]) === this.numeroDeSemanaDeUltimaAsistencia() - 1;
    }

    numeroDeSemanaDeUltimaAsistencia(){
        return this.getNumeroDeSemana(this.validarDia.getUltimaFechaDeAsistencia());
    }
    
   

    getNumeroDeSemana(date){
        return Math.floor(date.getDate()/7)+1;
    }

    
}

module.exports = FechaTipoComision;
