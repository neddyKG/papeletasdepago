class SindicatoFactory{
    
    constructor(perteneceSindicato){
        this.perteneceSindicato = perteneceSindicato;
        return this.crearSindicato();
    }

    crearSindicato(datosEmpleadoFijo) {
        let sindicato = null;
        if (datosEmpleadoFijo.sindicato === 'Pertenece')
           sindicato = new Pertenece();
        else
           sindicato = new NoPertenece();
        return sindicato;
     }
}

module.exports = SindicatoFactory;
