var moment = require('moment');

class TarjetaDeAsistencia {

    constructor (horaLlegada,horaSalida) {
        this.fecha = this.getTodayDate();
        this.asistencias = [];
        this.horaLlegada = new Date(horaLlegada);
        this.horaSalida = new Date(horaSalida);
    }

    getCantidadDeHoras () {
        return this.horaSalida.getHours() - this.horaLlegada.getHours();
    }

    getTodayDate () {
        return new Date().toLocaleDateString();
    }
    
    addAsistencia(fecha) {
        this.asistencias.push(fecha);
    }

    getHoraLlegada() {
        return this.horaLlegada.toString();
    }

    getHoraSalida() {
        return this.horaSalida.toString();
    }

    getAsistencias() {
        return this.asistencias;
    }

}

module.exports = TarjetaDeAsistencia;