class PapeletaDePago{

    constructor(empleado){
        this.nombre = empleado.nombre;
        this.salario = empleado.calcularSalario();
        this.fechaDeEmision = this.getTodaysDate();
    }

    generarPapeletaDePago(){
        var papeleta = {Nombre :  this.nombre, Salario : this.salario, Fecha : this.fechaDeEmision};
        return papeleta;
    }

    getTodaysDate(){
        return new Date().toLocaleDateString();
    }
}


module.exports = PapeletaDePago;
