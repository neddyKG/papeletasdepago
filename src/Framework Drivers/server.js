let express = require('express');
var cors = require('cors');
let app = express();
let EmpleadoController = require('../Interface Adapters/Controllers/EmpleadoController');
let GenerarPapeletasController = require('../Interface Adapters/Controllers/GenerarPapeletasController');
let RespositorioEmpleado = require('../Logica del Negocio/Use Cases/RepositorioEmpleado');
let firebaseEmpleadoFijo = require('../Framework Drivers/Database/Models/Empleado/EmpleadoFijo');
let firebaseEmpleadoComision = require('../Framework Drivers/Database/Models/Empleado/EmpleadoPorComision');
let firebaseEmpleadoPorHora = require('../Framework Drivers/Database/Models/Empleado/EmpleadoPorHora');
let firebaseEmpleado = require('../Framework Drivers/Database/Models/Empleado/Empleado');

const repositorioEmpleadoFijo = new RespositorioEmpleado(firebaseEmpleadoFijo);
const repositorioEmpleadoComision = new RespositorioEmpleado(firebaseEmpleadoComision);
const repositorioEmpleadoHora = new RespositorioEmpleado(firebaseEmpleadoPorHora);
const repositorioEmpleado = new RespositorioEmpleado(firebaseEmpleado);
let empleadoController = new EmpleadoController();
let generarPapeletasController = new GenerarPapeletasController();

var bodyParser = require('body-parser');
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors());


app.get('/', function (req, res) {
  res.send('BIENVENIDO!!!');  
});


app.post('/empleadoFijo/nuevo', bodyParser.json(),function (req, res) {
  empleadoController.almacenarEmpleadoFijo(req, res, repositorioEmpleadoFijo);
});


app.post('/empleadoComision/nuevo', function (req, res) {
  empleadoController.almacenarEmpleadoComision(req, res, repositorioEmpleadoComision);
});

app.post('/empleadoHora/nuevo', function (req, res) {
  empleadoController.almacenarEmpleadoHora(req, res, repositorioEmpleadoHora);
});

app.get('/empleadosFijos', function (req, res) {
  res.send('lista de empleados fijos!');  
});

app.get('/generarPapeletaDePago', function (req, res) {
  generarPapeletasController.generarPapeletasDePago(req, res, repositorioEmpleado);
});

app.listen(3000, function () {
  console.log('listening on port 3000!');
});


