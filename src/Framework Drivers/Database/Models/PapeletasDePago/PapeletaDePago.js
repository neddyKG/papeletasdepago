const database = require('../../Firebase/FirebaseConfig')
let PapeletaDePago = require('../../../../Logica del Negocio/Use Cases/GeneradorPapeletasDePago');

const dbRef = database.ref();
const papeletaDePagosRef = dbRef.child('papeletaDePagos');

const persistirPapeletaDePago = (papeletaDePago) => {
    return new Promise((resolve, reject) => {
        papeletaDePagosRef.push({
          papeleta: papeletaDePago.generarPapeletasDePago()
          }).then(result => {
            resolve(true);
          }).catch(err => {
            reject(false);
        });
    });
};

var listaPapeletaDePagos = () => {
    return new Promise((resolve, reject) => {
        var papeletasList = [];
        papeletaDePagosRef.once('value', snapshot => {
          snapshot.forEach(child => {
            papeletasList.push(
              child.val().papeleta
            );
          });
          resolve(papeletasList);
        });
    })
};



module.exports = {persistirPapeletaDePago, listaPapeletaDePagos};