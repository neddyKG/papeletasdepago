const database = require('../../Firebase/FirebaseConfig');

const dbRef = database.ref();
const empleadosRef = dbRef.child('empleados');
const porComisionRef = empleadosRef.child('porComision');

let Empleado = require('../../../../Logica del Negocio/Entidades/Empleado/Empleado');
let TarjetaDeAsistencia = require('../../../../Logica del Negocio/Entidades/TarjetaDeAsistencia');
let TarjertaDeVenta = require('../../../../Logica del Negocio/Entidades/TarjetaDeVenta');
let SalarioComision = require('../../../../Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioComision');
let FechaDePago = require('../../../../Logica del Negocio/Entidades/Empleado/Fecha de Pago Strategy/FechaTipoComision');
let MetodoDePagoFactory = require('../../../../Logica del Negocio/Entidades/Empleado/MetodoDePagoFactory');
let MediosDeNotificacionFactory = require('../../../../Logica del Negocio/Entidades/Empleado/MediosDeNotificacionFactory');
let Pertenece = require('../../../../Logica del Negocio/Entidades/Empleado/Sindicato Strategy/Pertenece');
let NoPertenece = require('../../../../Logica del Negocio/Entidades/Empleado/Sindicato Strategy/NoPertenece');

const persistirEmpleado = (empleado) => {
    return new Promise((resolve, reject) => {
      porComisionRef.push({
        nombre: empleado.nombre,
        ci: empleado.ci,
        salario: empleado.calcularSalario(),
        comision: empleado.calculadoraSalario.getPorcentajeComision(),
        ventas: empleado.calculadoraSalario.getVenta().getVentas(),
        horaLlegada: empleado.calculadoraSalario.getAsistencia().getHoraLlegada(),
        horaSalida: empleado.calculadoraSalario.getAsistencia().getHoraSalida(),
        asistencias: empleado.calculadoraSalario.getAsistencia().getAsistencias(),
        mediosDeNotificacion: empleado.mediosDeNotificacion.obtenerMediosDeNotificacionEmpleado(),
        metodoDePago: empleado.obtenerPago(),
        celular: empleado.celular,
        email: empleado.email,
        sindicato: empleado.perteneceAlSindicato()
        }).then(result => {
            resolve(true);
        }).catch(err => {
          reject(false);
        });
    });
};

var listaEmpleados = () => {
  return new Promise((resolve, reject) => {
    var empleadosList = [];
    var empleado;
    porComisionRef.once('value', snapshot => {
      snapshot.forEach(child => {
        let asistencia = new TarjetaDeAsistencia(child.val().horaLlegada, child.val().horaSalida);
        child.val().asistencias.forEach(asistenciadb => {
          asistencia.addAsistencia(new Date(asistenciadb));
        });
        let ventas = new TarjertaDeVenta();
        child.val().ventas.forEach(venta => {
          ventas.addVenta(venta.fecha, venta.monto);
        });
        let sindicato;
        if (child.val().sindicato) {
          sindicato = new Pertenece();
        } else {
          sindicato = new NoPertenece();
        }
        let salario = new SalarioComision(child.val().salario, child.val().comision, ventas, asistencia, asistencia);
        let metodoDePago = new MetodoDePagoFactory(child.val().metodoDePago);
        let mediosDeNotificacionEmpleado = new MediosDeNotificacionFactory(child.val().mediosDeNotificacion, child.val().celular,
        child.val().email);
        let fechaDePago = new FechaDePago(salario);
        empleado = new Empleado(child.val().nombre, 
          child.val().ci,
          fechaDePago,
          salario,
          metodoDePago,
          mediosDeNotificacionEmpleado,
          child.val().celular,
          child.val().email, 
          sindicato);
        empleadosList.push(
          empleado
        );
      });
      resolve(empleadosList);
    });
  })
}; 

var listaEmpleadosCorrespondePagar = (fecha) => {
  var lista = [];
  listaEmpleados().then(function(result) {
    result.forEach(empleado => {
      if (empleado.correspondePagar() === fecha) {
        lista.push(empleado);
      }
    });
  }, function(err) {
    console.log(err); // Error: "It broke"
  });
  return lista;
};

module.exports = {persistirEmpleado, listaEmpleados, listaEmpleadosCorrespondePagar};