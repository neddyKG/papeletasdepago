const database = require('../../Firebase/FirebaseConfig');

const dbRef = database.ref();
const empleadosRef = dbRef.child('empleados');
const fijoRef = empleadosRef.child('fijo');

let Empleado = require('../../../../Logica del Negocio/Entidades/Empleado/Empleado');
let TarjetaDeAsistencia = require('../../../../Logica del Negocio/Entidades/TarjetaDeAsistencia');
let SalarioFijo = require('../../../../Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioFijo');
let FechaDePago = require('../../../../Logica del Negocio/Entidades/Empleado/Fecha de Pago Strategy/FechaTipoFijo');
let MetodoDePagoFactory = require('../../../../Logica del Negocio/Entidades/Empleado/MetodoDePagoFactory');
let MediosDeNotificacionFactory = require('../../../../Logica del Negocio/Entidades/Empleado/MediosDeNotificacionFactory');
let Pertenece = require('../../../../Logica del Negocio/Entidades/Empleado/Sindicato Strategy/Pertenece');
let NoPertenece = require('../../../../Logica del Negocio/Entidades/Empleado/Sindicato Strategy/NoPertenece');

const persistirEmpleado = (empleado) => {
    return new Promise((resolve, reject) => {
      fijoRef.push({
        nombre: empleado.nombre,
        ci: empleado.ci,
        salario: empleado.calcularSalario(),
        horaLlegada: empleado.calculadoraSalario.getAsistencia().getHoraLlegada(),
        horaSalida: empleado.calculadoraSalario.getAsistencia().getHoraSalida(),
        asistencias: empleado.calculadoraSalario.getAsistencia().getAsistencias(),
        mediosDeNotificacion: empleado.mediosDeNotificacion.obtenerMediosDeNotificacionEmpleado(),
        metodoDePago: empleado.obtenerPago(),
        celular: empleado.celular,
        email: empleado.email,
        sindicato: empleado.perteneceAlSindicato()
        }).then(result => {
            resolve(true);
        }).catch(err => {
          reject(false);
        });
    });
};

var listaEmpleados = () => {
  return new Promise((resolve, reject) => {
    var empleadosList = [];
    var empleado;
    fijoRef.once('value', snapshot => {
      snapshot.forEach(child => {
        let asistencia = new TarjetaDeAsistencia(child.val().horaLlegada, child.val().horaSalida);
        child.val().asistencias.forEach(asistenciadb => {
          asistencia.addAsistencia(new Date(asistenciadb));
        });
        let sindicato;
        if (child.val().sindicato) {
          sindicato = new Pertenece();
        } else {
          sindicato = new NoPertenece();
        }
        let salarioFijo = new SalarioFijo(child.val().salario, asistencia, sindicato);
        let metodoDePago = new MetodoDePagoFactory(child.val().metodoDePago);
        let mediosDeNotificacionEmpleado = new MediosDeNotificacionFactory(child.val().mediosDeNotificacion, child.val().celular,
        child.val().email);
        let fechaDePago = new FechaDePago(salarioFijo);
        empleado = new Empleado(child.val().nombre, 
          child.val().ci,
          fechaDePago,
          salarioFijo,
          metodoDePago,
          mediosDeNotificacionEmpleado,
          child.val().celular,
          child.val().email, 
          sindicato);
        empleadosList.push(
          empleado
        );
      });
      resolve(empleadosList);
    });
  })
}; 

var listaEmpleadosCorrespondePagar = (fecha) => {
  var lista = [];
  listaEmpleados().then(function(result) {
    result.forEach(empleado => {
      if (empleado.correspondePagar() === fecha) {
        lista.push(empleado);
      }
    });
  }, function(err) {
    console.log(err); // Error: "It broke"
  });
  setTimeout(function() {
    return lista;
  }, 2000);
};

module.exports = {persistirEmpleado, listaEmpleados, listaEmpleadosCorrespondePagar};