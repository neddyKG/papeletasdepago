const database = require('../../Firebase/FirebaseConfig');

let Empleado = require('../../../../Logica del Negocio/Entidades/Empleado/Empleado');
let TarjetaDeAsistencia = require('../../../../Logica del Negocio/Entidades/TarjetaDeAsistencia');
let TarjertaDeVenta = require('../../../../Logica del Negocio/Entidades/TarjetaDeVenta');
let SalarioFijo = require('../../../../Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioFijo');
let SalarioPorHora = require('../../../../Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioPorHora');
let SalarioComision = require('../../../../Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioComision');
let FechaTipoComision = require('../../../../Logica del Negocio/Entidades/Empleado/Fecha de Pago Strategy/FechaTipoComision');
let FechaTipoPorHora = require('../../../../Logica del Negocio/Entidades/Empleado/Fecha de Pago Strategy/FechaTipoPorHora');
let FechaTipoFijo = require('../../../../Logica del Negocio/Entidades/Empleado/Fecha de Pago Strategy/FechaTipoFijo');
let MetodoDePagoFactory = require('../../../../Logica del Negocio/Entidades/Empleado/MetodoDePagoFactory');
let MediosDeNotificacionFactory = require('../../../../Logica del Negocio/Entidades/Empleado/MediosDeNotificacionFactory');
let Pertenece = require('../../../../Logica del Negocio/Entidades/Empleado/Sindicato Strategy/Pertenece');
let NoPertenece = require('../../../../Logica del Negocio/Entidades/Empleado/Sindicato Strategy/NoPertenece');

const dbRef = database.ref();
const empleadosRef = dbRef.child('empleados');

const persistirEmpleado = (empleado) => {};
var listaEmpleadosCorrespondePagar = (fecha) => {
    return new Promise((resolve, reject) => {
        var empleados = [];
        empleadosRef.once('value', snapshot => {
            snapshot.forEach(child => {
                if(child.key === 'fijo') {
                    child.forEach(child => {
                        let asistencia = new TarjetaDeAsistencia(child.val().horaLlegada, child.val().horaSalida);
                        child.val().asistencias.forEach(asistenciadb => {
                          asistencia.addAsistencia(new Date(asistenciadb));
                        });
                        let sindicato;
                        if (child.val().sindicato) {
                          sindicato = new Pertenece();
                        } else {
                          sindicato = new NoPertenece();
                        }
                        let salarioFijo = new SalarioFijo(child.val().salario, asistencia, sindicato);
                        let metodoDePago = new MetodoDePagoFactory(child.val().metodoDePago);
                        let mediosDeNotificacionEmpleado = new MediosDeNotificacionFactory(child.val().mediosDeNotificacion, child.val().celular,
                        child.val().email);
                        let fechaDePago = new FechaTipoFijo(salarioFijo);
                        empleado = new Empleado(child.val().nombre, 
                          child.val().ci,
                          fechaDePago,
                          salarioFijo,
                          metodoDePago,
                          mediosDeNotificacionEmpleado,
                          child.val().celular,
                          child.val().email,
                          sindicato);
                          if(empleado.correspondePagar() === fecha) {
                            empleados.push(empleado);
                          }
                    });
                }
                if(child.key === 'porHora') {
                    child.forEach(child => {
                        let asistencia = new TarjetaDeAsistencia(child.val().horaLlegada, child.val().horaSalida);
                          child.val().asistencias.forEach(asistenciadb => {
                            asistencia.addAsistencia(new Date(asistenciadb));
                          });
                          let sindicato;
                          if (child.val().sindicato) {
                            sindicato = new Pertenece();
                          } else {
                            sindicato = new NoPertenece();
                          }
                          let salario = new SalarioPorHora(child.val().salario, asistencia, sindicato);
                          let metodoDePago = new MetodoDePagoFactory(child.val().metodoDePago);
                          let mediosDeNotificacionEmpleado = new MediosDeNotificacionFactory(child.val().mediosDeNotificacion, child.val().celular,
                          child.val().email);
                          let fechaDePago = new FechaTipoPorHora(salario);
                          empleado = new Empleado(child.val().nombre, 
                            child.val().ci,
                            fechaDePago,
                            salario,
                            metodoDePago,
                            mediosDeNotificacionEmpleado,
                            child.val().celular,
                            child.val().email,
                            sindicato);
                            if(empleado.correspondePagar() === fecha) {
                              empleados.push(empleado);
                            }
                        });
                }
                if(child.key === 'porComision') {
                    child.forEach(child => {
                        let asistencia = new TarjetaDeAsistencia(child.val().horaLlegada, child.val().horaSalida);
                        child.val().asistencias.forEach(asistenciadb => {
                        asistencia.addAsistencia(new Date(asistenciadb));
                        });
                        let ventas = new TarjertaDeVenta();
                        child.val().ventas.forEach(venta => {
                          ventas.addVenta(venta.fecha, venta.monto);
                        });
                        let sindicato;
                        if (child.val().sindicato) {
                          sindicato = new Pertenece();
                        } else {
                          sindicato = new NoPertenece();
                        }
                        let salario = new SalarioComision(child.val().salario, child.val().comision, ventas, asistencia, sindicato);
                        let metodoDePago = new MetodoDePagoFactory(child.val().metodoDePago);
                        let mediosDeNotificacionEmpleado = new MediosDeNotificacionFactory(child.val().mediosDeNotificacion, child.val().celular,
                        child.val().email);
                        let fechaDePago = new FechaTipoComision(salario);
                        empleado = new Empleado(child.val().nombre, 
                          child.val().ci,
                          fechaDePago,
                          salario,
                          metodoDePago,
                          mediosDeNotificacionEmpleado,
                          child.val().celular,
                          child.val().email,
                          sindicato);
                          if(empleado.correspondePagar() === fecha) {
                            empleados.push(empleado);
                          }
                    });
                }
            });
            resolve(empleados);
        });
    });
};

module.exports = {persistirEmpleado, listaEmpleadosCorrespondePagar};