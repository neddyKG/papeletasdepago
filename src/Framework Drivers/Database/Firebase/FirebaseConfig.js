const firebase = require("firebase");

const config = {
    apiKey: "AIzaSyCnTlD89Z5kktEeCCNOY_QzjGtwxDTRqlw",
    authDomain: "papeletasdepago.firebaseapp.com",
    databaseURL: "https://papeletasdepago.firebaseio.com",
    projectId: "papeletasdepago",
    storageBucket: "papeletasdepago.appspot.com",
    messagingSenderId: "303242710486"
};
firebase.initializeApp(config);

const database = firebase.database();
 
module.exports = database;