class ModeloPeticionEmpleadoComision{
    modelarEmpleadoComision(empleado){
        return ({
            nombre: empleado.nombre,
            ci: empleado.ci,
            salario: empleado.salario,
            comision: empleado.comision,
            ventas: empleado.ventas,
            horaLlegada: empleado.horaLlegada,
            horaSalida: empleado.horaSalida,
            asistencia: null,
            mediosDeNotificacion: empleado.mediosDeNotificacion,
            metodoDePago: empleado.metodoDePago,
            celular: empleado.celular,
            email: empleado.email,
            sindicato: empleado.sindicato
    
        })
    }
}

module.exports = ModeloPeticionEmpleadoComision;