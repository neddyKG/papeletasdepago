class ModeloPeticionEmpleadoHora{
    modelarEmpleadoHora(empleado){
        return ({
            nombre: empleado.nombre,
            ci: empleado.ci,
            salario: empleado.salario,
            horaLlegada: empleado.horaLlegada,
            horaSalida: empleado.horaSalida,
            asistencia: null,
            mediosDeNotificacion: empleado.mediosDeNotificacion,
            metodoDePago: empleado.metodoDePago,
            celular: empleado.celular,
            email: empleado.email
    
        })
    }
}

module.exports = ModeloPeticionEmpleadoHora;