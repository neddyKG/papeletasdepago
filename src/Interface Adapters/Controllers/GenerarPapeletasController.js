let ModeloPeticionGenerarPapeletas = require('../Modelo Peticion/ModeloPeticionGenerarPapeletasDePago');
let ModeloPresentacionGenerarPapeletasDePago = require('../Modelo Presentacion/ModeloPresentacionGenerarPapeletasDePago');
let GenerarPapeletasDePago = require('../../Logica del Negocio/Use Cases/GeneradorPapeletasDePago');

class GenerarPapeletasController {
    constructor() {}
    
    generarPapeletasDePago(req, res, repositorioEmpleado){
        let modeloPeticionGenerarPapeletas = new ModeloPeticionGenerarPapeletas();
        repositorioEmpleado.obtenerListaEmpleadosCorrespondePagar(modeloPeticionGenerarPapeletas.modelarPapeletasDePago(req.query).fecha).then(function(result) {
            let generarPapeletasDePago = new GenerarPapeletasDePago(result);
            let papeletas = generarPapeletasDePago.generarPapeletasDePago();
            let modeloPresentacionGenerarPapeletasDePago = new ModeloPresentacionGenerarPapeletasDePago();
            modeloPresentacionGenerarPapeletasDePago.modelarPapeletDePago(res, papeletas);
        }, function(err) {
            console.log(err);
        });
    }
}

module.exports = GenerarPapeletasController;