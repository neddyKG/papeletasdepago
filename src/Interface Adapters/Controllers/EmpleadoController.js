let ModeloPeticionEmpleadoFijo = require('../Modelo Peticion/ModeloPeticionEmpleadoFijo');
let ModeloPeticionEmpleadoComision = require('../Modelo Peticion/ModeloPeticionEmpleadoComision');
let ModeloPeticionEmpleadoHora = require('../Modelo Peticion/ModeloPeticionEmpleadoHora');
let CrearEmpleado = require('../../Logica del Negocio/Use Cases/CrearEmpleado');
let ModeloPresentacionEmpleado = require('../Modelo Presentacion/ModeloPresentacionEmpleado');

class EmpleadoController{

     almacenarEmpleadoFijo(req, res, repositorioEmpleado) {
        let modeloPeticionEmpleado = new ModeloPeticionEmpleadoFijo();
        let crearEmpleado = new CrearEmpleado();
        let modeloEmpleado = modeloPeticionEmpleado.modelarEmpleadoFijo(req.body);
        crearEmpleado.crearEmpleadoFijo(modeloEmpleado, repositorioEmpleado);
        let modeloPresentacionEmpleado = new ModeloPresentacionEmpleado();
        modeloPresentacionEmpleado.enviarRespuesta(res);
      }

      almacenarEmpleadoComision(req, res, repositorioEmpleado) {
        let modeloPeticionEmpleado = new ModeloPeticionEmpleadoComision();
        let crearEmpleado = new CrearEmpleado();
        crearEmpleado.crearEmpleadoComision(modeloPeticionEmpleado.modelarEmpleadoComision(req.body), repositorioEmpleado);
        let modeloPresentacionEmpleado = new ModeloPresentacionEmpleado();
        modeloPresentacionEmpleado.enviarRespuesta(res);
      }

      almacenarEmpleadoHora(req, res, repositorioEmpleado) {
        let modeloPeticionEmpleado = new ModeloPeticionEmpleadoHora();
        let crearEmpleado = new CrearEmpleado();
        crearEmpleado.crearEmpleadoHora(modeloPeticionEmpleado.modelarEmpleadoHora(req.body), repositorioEmpleado);
        let modeloPresentacionEmpleado = new ModeloPresentacionEmpleado();
        modeloPresentacionEmpleado.enviarRespuesta(res);
      }
       /*var empleadosFijos = repositorioEmpleadoFijo.listaEmpleadosCorrespondePagar(req).then(function(result) {
            return result; 
          }, function(err) {
            console.log(err);
          });*/
}

module.exports = EmpleadoController;