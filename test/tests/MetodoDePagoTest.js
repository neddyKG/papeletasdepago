var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

let TarjetaDeAsistencia = require ('../../src/Logica del Negocio/Entidades/TarjetaDeAsistencia');
let Empleado = require ('../../src/Logica del Negocio/Entidades/Empleado/Empleado');
let SalarioFijo = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioFijo');
let MetodoDePago = require ('../../src/Logica del Negocio/Entidades/Empleado/Metodo de Pago Strategy/MetodoDePago');
let CalculadoraSalario = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/CalculadoraSalario');
let PagoEnCheque = require ('../../src/Logica del Negocio/Entidades/Empleado/Metodo de Pago Strategy/EnCheque');
let PagoEnDeposito = require ('../../src/Logica del Negocio/Entidades/Empleado/Metodo de Pago Strategy/EnDeposito');
let PagoEnEfectivo = require ('../../src/Logica del Negocio/Entidades/Empleado/Metodo de Pago Strategy/EnEfectivo');

describe('Test de Metodo de Pago', function() {

    beforeEach(() => {
        asistencia = new TarjetaDeAsistencia("07:30 AM", "11:30 AM");
    });

    it('deberia obtener el pago en cheque', function() {
        let salarioFijo = new SalarioFijo(1000, asistencia);
        let calculadoraSalario = new CalculadoraSalario(salarioFijo);
        let pagoencheque = new PagoEnCheque ();
        
        let empleado = new Empleado("Antonio", 8451717, null, calculadoraSalario, pagoencheque);
        expect(empleado.obtenerPago()).equal("Pago en Cheque");
    });


    it('deberia obtener su deposito correspondiente', function() {
        let salarioFijo = new SalarioFijo(1000, asistencia);
        let calculadoraSalario = new CalculadoraSalario(salarioFijo);
        let pagoendeposito = new PagoEnDeposito();
        let empleado = new Empleado("Antonio", 8451717, null, calculadoraSalario, pagoendeposito);
        expect(empleado.obtenerPago()).equal("Pago en Deposito");
    });

    it('deberia obtener su pago en efectivo', function() {
        let salarioFijo = new SalarioFijo(1000, asistencia);
        let calculadoraSalario = new CalculadoraSalario(salarioFijo);
        let pagoenefectivo = new PagoEnEfectivo();
        let empleado = new Empleado("Antonio", 8451717, null, calculadoraSalario, pagoenefectivo);
        expect(empleado.obtenerPago()).equal("Pago en Efectivo");
    });

});
