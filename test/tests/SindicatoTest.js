var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

let TarjetaDeAsistencia = require ('../../src/Logica del Negocio/Entidades/TarjetaDeAsistencia');
let Empleado = require ('../../src/Logica del Negocio/Entidades/Empleado/Empleado');
let SalarioFijo = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioFijo');
let SalarioPorHora = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioPorHora');
let SalarioComision = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioComision');
let CalculadoraSalario = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/CalculadoraSalario');
let NoPertenece = require('../../src/Logica del Negocio/Entidades/Empleado/Sindicato Strategy/NoPertenece');
let Pertenece = require('../../src/Logica del Negocio/Entidades/Empleado/Sindicato Strategy/Pertenece');

describe('Test del Sindicato de Empleados', function() {

    beforeEach(() => {
        asistencia = new TarjetaDeAsistencia("07:30 AM", "11:30 AM");
    });

    it('deberia pertenecer el empleado salario fijo al sindicato', function() {
        let sindicato = new Pertenece()
        let salarioFijo = new SalarioFijo(1000, asistencia, sindicato);
        let calculadoraSalario = new CalculadoraSalario(salarioFijo);
        let empleado = new Empleado("Antonio", 8451717, null, calculadoraSalario, null, null, null, null, sindicato);
        expect(empleado.perteneceAlSindicato()).equal(true);
    });

    it('deberia pertenecer el empleado salario comision al sindicato', function() {
        let sindicato = new Pertenece()
        let salarioComision = new SalarioComision(1000, asistencia, sindicato);
        let calculadoraSalario = new CalculadoraSalario(salarioComision);
        let empleado = new Empleado("Antonio", 8451717, null, calculadoraSalario, null, null, null, null, sindicato);
        expect(empleado.perteneceAlSindicato()).equal(true);
    });

    it('deberia no pertenecer el empleado salario fijo al sindicato', function() {
        let sindicato = new NoPertenece()
        let salarioFijo = new SalarioFijo(1000, asistencia, sindicato);
        let calculadoraSalario = new CalculadoraSalario(salarioFijo);
        let empleado = new Empleado("Antonio", 8451717, null, calculadoraSalario, null, null, null, null, sindicato);
        expect(empleado.perteneceAlSindicato()).equal(false);
    });

    it('deberia no pertenecer el empleado salario comision al sindicato', function() {
        let sindicato = new NoPertenece()
        let salarioComision = new SalarioComision(1000, asistencia, sindicato);
        let calculadoraSalario = new CalculadoraSalario(salarioComision);
        let empleado = new Empleado("Antonio", 8451717, null, calculadoraSalario, null, null, null, null, sindicato);
        expect(empleado.perteneceAlSindicato()).equal(false);
    });
});