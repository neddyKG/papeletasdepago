var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

let Empleado = require('../../src/Logica del Negocio/Entidades/Empleado/Empleado');

let Email = require('../../src/Logica del Negocio/Entidades/Empleado/Medios de Notificacion Composite/Email');
let Facebook = require('../../src/Logica del Negocio/Entidades/Empleado/Medios de Notificacion Composite/Facebook');
let SMS = require('../../src/Logica del Negocio/Entidades/Empleado/Medios de Notificacion Composite/SMS');
let WhatsApp = require('../../src/Logica del Negocio/Entidades/Empleado/Medios de Notificacion Composite/WhatsApp');
let MediosDeNotificacion = require('../../src/Logica del Negocio/Entidades/Empleado/Medios de Notificacion Composite/MediosDeNotificacion');
let SalarioFijo = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioFijo');
let PapeletaDePago = require('../../src/Logica del Negocio/Entidades/Papeleta de Pago/PapeletaDePago');
let Pertenece = require('../../src/Logica del Negocio/Entidades/Empleado/Sindicato Strategy/Pertenece');
let NoPertenece = require('../../src/Logica del Negocio/Entidades/Empleado/Sindicato Strategy/NoPertenece');

describe('Test de medios de notificacion que elige el empleado', function() {
    let empleado, salarioFijo, sindicato;
    beforeEach(() => {
        sindicato = new NoPertenece();
        todaysDate = new Date().toLocaleDateString();
        salarioFijo = new SalarioFijo(1000, null, sindicato);
    });

    it('deberia devolver whatsapp como medios de notificacion del empleado', function() {
        let whatsapp = new WhatsApp('79389772');
        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(whatsapp);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        expect(mediosDeNotificacionEmpleado.obtenerMediosDeNotificacionEmpleado()).eql([ 'Whatsapp' ]); 
    });

    //new

    it('deberia devolver sms como medios de notificacion del empleado', function() {
        let sms = new SMS('79389772');
        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(sms);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        expect(mediosDeNotificacionEmpleado.obtenerMediosDeNotificacionEmpleado()).eql([ 'SMS' ]);
    });

    it('deberia devolver email como medios de notificacion del empleado', function() {
        let email = new Email('n.gonzalez@empresasim.com');

        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(email);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        expect(mediosDeNotificacionEmpleado.obtenerMediosDeNotificacionEmpleado()).eql([ 'Email' ]);
    });

    it('deberia devolver facebook como medios de notificacion del empleado', function() {
        let facebook = new Facebook('n.gonzalez@empresasim.com');

        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(facebook);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        expect(mediosDeNotificacionEmpleado.obtenerMediosDeNotificacionEmpleado()).eql([ 'Facebook' ]);
    });


    it('deberia devolver whatsapp y email como medios de notificacion del empleado', function() {
        let whatsapp = new WhatsApp('79389772');
        let email = new Email('n.gonzalez@empresasim.com');

        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(whatsapp);
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(email);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        expect(mediosDeNotificacionEmpleado.obtenerMediosDeNotificacionEmpleado()).eql([ 'Whatsapp',  'Email' ]);
    });


    it('deberia devolver todos los medios de notificacion ya que el empleado eligio todos', function() {
        let whatsapp = new WhatsApp('79389772');
        let email = new Email('n.gonzalez@empresasim.com');
        let sms = new SMS('79389772');
        let facebook = new Facebook('n.gonzalez@empresasim.com');

        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(whatsapp);
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(email);
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(sms);
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(facebook);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        expect(mediosDeNotificacionEmpleado.obtenerMediosDeNotificacionEmpleado()).eql([ 'Whatsapp', 'Email', 'SMS', 'Facebook' ]);
    });

    it('deberia enviar por email su papeleta de pago de un empleado', function() {
        let email = new Email('n.gonzalez@empresasim.com');
        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(email);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        let papeletaDePago = new PapeletaDePago(empleado);
        expect(mediosDeNotificacionEmpleado.enviarNotificacion(papeletaDePago)).equal('Enviado ');
    });

    //////
    it('deberia enviar por facebook su Empleadopapeleta de pago de un empleado', function() {
        let facebook = new Facebook('n.gonzalez@empresasim.com');
        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(facebook);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        let papeletaDePago = new PapeletaDePago(empleado);
        expect(mediosDeNotificacionEmpleado.enviarNotificacion(papeletaDePago)).equal('Notificacion Facebook ');
    });


    it('deberia enviar por whatsapp su papeleta de pago de un empleado', function() {
        let whatsapp = new WhatsApp('79389772');
        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(whatsapp);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        let papeletaDePago = new PapeletaDePago(empleado);
        expect(mediosDeNotificacionEmpleado.enviarNotificacion(papeletaDePago)).equal('Notificacion Whatsapp ');
    });

    it('deberia enviar por whatsapp su papeleta de pago de un empleado', function() {
        let sms = new SMS('79389772');
        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(sms);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        let papeletaDePago = new PapeletaDePago(empleado);
        mediosDeNotificacionEmpleado.obtenerPapeletaDePago(papeletaDePago);
        expect(mediosDeNotificacionEmpleado.enviarNotificacion(papeletaDePago)).equal('Notificacion SMS ');
    });

    it('deberia enviar por whatsapp su papeleta de pago de un empleado', function() {
        let sms = new SMS('79389772');
        let whatsapp = new WhatsApp('79389772');

        let mediosDeNotificacionEmpleado = new MediosDeNotificacion();
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(sms);
        mediosDeNotificacionEmpleado.agregarMedioDeNotificacion(whatsapp);
        empleado = new Empleado("Neddy Gonzalez", 8451717, null, salarioFijo, null, mediosDeNotificacionEmpleado);
        let papeletaDePago = new PapeletaDePago(empleado);
        mediosDeNotificacionEmpleado.obtenerPapeletaDePago(papeletaDePago);
        expect(mediosDeNotificacionEmpleado.enviarNotificacion(papeletaDePago)).equal('Notificacion SMS Notificacion Whatsapp ');
    });

});
