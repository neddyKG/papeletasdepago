var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

let TarjetaDeAsistencia = require('../../src/Logica del Negocio/Entidades/TarjetaDeAsistencia');
let Empleado = require('../../src/Logica del Negocio/Entidades/Empleado/Empleado');
let SalarioFijo = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioFijo');
let SalarioPorHora = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioPorHora');
let PapeletaDePago = require('../../src/Logica del Negocio/Entidades/Papeleta de Pago/PapeletaDePago');
let Pertenece = require('../../src/Logica del Negocio/Entidades/Empleado/Sindicato Strategy/Pertenece');
let NoPertenece = require('../../src/Logica del Negocio/Entidades/Empleado/Sindicato Strategy/NoPertenece');

describe('Test de Asistencia', function() {

    beforeEach(() => {
        todaysDate = new Date().toLocaleDateString();
    });

    it('deberia devolver la papeleta de pago para un empleado de Salario fijo', function() {
        let asistencia = new TarjetaDeAsistencia("April 10, 2019 07:30 AM", "April 10, 2019 11:30 AM");
        let sindicato = new Pertenece();
        let salarioFijo = new SalarioFijo(1000, asistencia, sindicato);
        let empleado = new Empleado("Jose", 8451717, null, salarioFijo, null, null, 7564852, "jose@gmail.com", sindicato);
        let papeletaDePago = new PapeletaDePago(empleado);
        expect(papeletaDePago.nombre).equal(empleado.nombre);
    });

    it('deberia devolver la papeleta de pago para un empleado de Salario por hora', function() {
        let asistencia = new TarjetaDeAsistencia("April 10, 2019 07:30 AM", "April 10, 2019 11:30 AM");
        let sindicato = new NoPertenece();
        let salarioPorHora = new SalarioPorHora(1000, asistencia, sindicato);
        let empleado = new Empleado("Pepe", 8451717, null, salarioPorHora, null, null, 7564852, "jose@gmail.com", sindicato);
        let papeletaDePago = new PapeletaDePago(empleado);
        expect(papeletaDePago.nombre).equal(empleado.nombre);
    });

});
