var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

let Venta = require('../../src/Logica del Negocio/Entidades/TarjetaDeVenta');
let Empleado = require('../../src/Logica del Negocio/Entidades/Empleado/Empleado');
let SalarioComision = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioComision');
let PapeletaDePago = require('../../src/Logica del Negocio/Entidades/Papeleta de Pago/PapeletaDePago');
let Pertenece = require('../../src/Logica del Negocio/Entidades/Empleado/Sindicato Strategy/Pertenece');
let NoPertenece = require('../../src/Logica del Negocio/Entidades/Empleado/Sindicato Strategy/NoPertenece');

describe('Test de Venta', function() {

    beforeEach(() => {
        todaysDate = new Date().toLocaleDateString();
    });

    it('deberia devolver el monto de la venta', function() {
        let venta = new Venta();
        venta.addVenta('06-06-13', 100);
        expect(venta.getCantidadMonto()).equal(100);
    });

    it('deberia devolver la papeleta de pago para un empleado por comision de 2 ventas', function() {
        let venta = new Venta();
        venta.addVenta('06-06-13', 100);
        venta.addVenta('06-06-13', 100);
        let sindicato = new Pertenece();
        let salarioComision = new SalarioComision(180, 0.1, venta, null, sindicato);
        let empleado = new Empleado("Carlos", 8451717, null, salarioComision);
        let papeletaDePago = new PapeletaDePago(empleado);
        expect(papeletaDePago.salario).equal(50);
    });

    it('deberia devolver la papeleta de pago para un empleado por comision de 4 ventas', function() {
        let venta = new Venta();
        venta.addVenta('06-06-13', 100);
        venta.addVenta('06-06-13', 100);
        venta.addVenta('06-06-13', 100);
        venta.addVenta('06-06-13', 100);
        let sindicato = new NoPertenece();
        let salarioComision = new SalarioComision(8, 0.1, venta, null, sindicato);
        let empleado = new Empleado("Carlos", 8451717, null, salarioComision);
        let papeletaDePago = new PapeletaDePago(empleado);
        expect(papeletaDePago.salario).equal(48);
    });


});
