var expect = require('chai').expect;
let TarjetaDeAsistencia = require('../../src/Logica del Negocio/Entidades/TarjetaDeAsistencia');
let Venta = require('../../src/Logica del Negocio/Entidades/TarjetaDeVenta');

let FechaFijo = require('../../src/Logica del Negocio/Entidades/Empleado/Fecha de Pago Strategy/FechaTipoFijo');
let FechaComision = require('../../src/Logica del Negocio/Entidades/Empleado/Fecha de Pago Strategy/FechaTipoComision');
let FechaPorHora = require('../../src/Logica del Negocio/Entidades/Empleado/Fecha de Pago Strategy/FechaTipoPorHora');
let SalarioFijo = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioFijo');
let SalarioComision = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioComision');
let SalarioPorHora = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioPorHora');


describe('Test de Fecha de Pago', function() {
    let asistencia;
    beforeEach(() => {
        asistencia = new TarjetaDeAsistencia("07:30 AM", "11:30 AM");
    });

    it('deberia devolver true para un salaraio por hora tomando en cuenta que se le paga cada semana', function() {


        asistencia.addAsistencia(new Date("April 10, 2019 05:35:32"));
        asistencia.addAsistencia(new Date("April 11, 2019 05:35:32"));
        asistencia.addAsistencia(new Date("April 12, 2019 05:35:32"));

        let salarioPorHora = new SalarioPorHora(1000, asistencia);

        let fechaPorHora = new FechaPorHora(salarioPorHora);

        expect(fechaPorHora.correspondePagar()).equal("Fri Apr 12 2019");
    });

    it('deberia devolver no corresponde pagarle para un salario por hora que no termino la semana', function() {

        asistencia.addAsistencia(new Date("April 10, 2019 05:35:32"));
        asistencia.addAsistencia(new Date("April 11, 2019 05:35:32"));
        asistencia.addAsistencia(new Date("April 15, 2019 05:35:32"));

        let salarioPorHora = new SalarioPorHora(1000, asistencia);
        let fechaPorHora = new FechaPorHora(salarioPorHora);
        expect(fechaPorHora.correspondePagar()).equal("No corresponde pagarle!");
    });


    it('deberia devolver fecha de pago para un empleado por comision tomando en cuenta que se le paga cada 2 semanas', function() {
        asistencia.addAsistencia(new Date("April 03, 2019 05:35:32"));
        asistencia.addAsistencia(new Date("April 04, 2019 05:35:32"));

        asistencia.addAsistencia(new Date("April 11, 2019 05:35:32"));
        asistencia.addAsistencia(new Date("April 12, 2019 05:35:32"));

        let venta = new Venta();
        venta.addVenta('06-06-13', 100);
        venta.addVenta('06-06-13', 100);

        let salarioComision = new SalarioComision(1000, 8, venta, asistencia);
        let fechaComision = new FechaComision(salarioComision);
        expect(fechaComision.correspondePagar()).equal("Fri Apr 12 2019");
    });

    it('deberia devolver No corresponde pagarle para un empleado por comision tomando en cuenta que no termino la segunda semana', function() {
        asistencia.addAsistencia(new Date("April 03, 2019 05:35:32"));
        asistencia.addAsistencia(new Date("April 04, 2019 05:35:32"));

        asistencia.addAsistencia(new Date("April 11, 2019 05:35:32"));
        asistencia.addAsistencia(new Date("April 17, 2019 05:35:32"));

        let venta = new Venta();
        venta.addVenta('06-06-13', 100);
        venta.addVenta('06-06-13', 100);

        let salarioComision = new SalarioComision(1000, 8, venta, asistencia);
        let fechaComision = new FechaComision(salarioComision);
        expect(fechaComision.correspondePagar()).equal("No corresponde pagarle!");
    });

    it('deberia devolver No corresponde pagarle para un empleado por comision tomando en cuenta que no trabajo dos semanas', function() {
        asistencia.addAsistencia(new Date("April 11, 2019 05:35:32"));
        asistencia.addAsistencia(new Date("April 12, 2019 05:35:32"));

        let venta = new Venta();
        venta.addVenta('06-06-13', 100);
        venta.addVenta('06-06-13', 100);
        let salarioComision = new SalarioComision(1000, 8, venta, asistencia);
        let fechaComision = new FechaComision(salarioComision);
        expect(fechaComision.correspondePagar()).equal("No corresponde pagarle!");
    });

    it('deberia devolver fecha de pago para un empleado fijo tomando en cuenta que se le paga fin de mes', function() {
        asistencia.addAsistencia(new Date("April 30, 2019 00:00:00"));
        let salarioFijo = new SalarioFijo(1000, asistencia);
        let fechaFijo = new FechaFijo(salarioFijo);
        expect(fechaFijo.correspondePagar()).equal('Tue Apr 30 2019');
    });

    it('deberia devolver no corresponde pagarle para un empleado fijo que no termino el mes de trabajo', function() {
        asistencia.addAsistencia(new Date("April 20, 2019 05:35:32"));

        let salarioFijo = new SalarioFijo(1000, asistencia);
        let fechaFijo = new FechaFijo(salarioFijo);
        expect(fechaFijo.correspondePagar()).equal('No corresponde pagarle!');
    });

});
