var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

let Empleado = require('../../src/Logica del Negocio/Entidades/Empleado/Empleado');
let GeneradorPapeletasDePago = require('../../src/Logica del Negocio/Use Cases/GeneradorPapeletasDePago');
let SalarioFijo = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioFijo');
let PapeletaDePago = require('../../src/Logica del Negocio/Entidades/Papeleta de Pago/PapeletaDePago');
let Pertenece = require('../../src/Logica del Negocio/Entidades/Empleado/Sindicato Strategy/Pertenece');
let NoPertenece = require('../../src/Logica del Negocio/Entidades/Empleado/Sindicato Strategy/NoPertenece');

describe('Generar Varias Papeletas de Pago', function() {

    beforeEach(() => {
        todaysDate = new Date().toLocaleDateString();
    });

    it ('deberia devolver la papeleta de pago de 3 personas', function() {
        
        let sindicato = new Pertenece();
        let salarioFijoDeEmpleado1 = new SalarioFijo(3000, null, sindicato);
        let empleado1 = new Empleado("Jose", 8451717, null, salarioFijoDeEmpleado1);

        let salarioFijoDeEmpleado2 = new SalarioFijo(5000, null, sindicato);
        let empleado2 = new Empleado("Maria", 8451717, null, salarioFijoDeEmpleado2);

        let salarioFijoDeEmpleado3 = new SalarioFijo(9000, null, sindicato);
        let empleado3 = new Empleado("Pepe", 8451717, null, salarioFijoDeEmpleado3);
        
        let empleados = [];
        empleados.push(empleado1);
        empleados.push(empleado2);
        empleados.push(empleado3);
        let generar = new GeneradorPapeletasDePago(empleados);

        let papeletas = [];
        let papeleta1 = new PapeletaDePago(empleado1);
        papeletas.push(papeleta1.generarPapeletaDePago());
        let papeleta2 = new PapeletaDePago(empleado2);
        papeletas.push(papeleta2.generarPapeletaDePago());
        let papeleta3 = new PapeletaDePago(empleado3);
        papeletas.push(papeleta3.generarPapeletaDePago());
        expect(generar.generarPapeletasDePago().length).equal(papeletas.length);
    });


});
