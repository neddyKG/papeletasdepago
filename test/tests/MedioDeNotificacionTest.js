var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

let Empleado = require('../../src/Logica del Negocio/Entidades/Empleado/Empleado');
let Email = require('../../src/Logica del Negocio/Entidades/Empleado/Medios de Notificacion Composite/Email');
let WhatsApp = require('../../src/Logica del Negocio/Entidades/Empleado/Medios de Notificacion Composite/WhatsApp');
let SalarioFijo = require('../../src/Logica del Negocio/Entidades/Empleado/Salario Strategy/SalarioFijo');

describe('Test de medios de notificacion', function() {

    let empleado;
    beforeEach(() => {
        todaysDate = new Date().toLocaleDateString();
        let salarioFijo = new SalarioFijo(1000);
        empleado = new Empleado("Maria", 8451717, null, salarioFijo, null, null, '79389772', 'maria@gmail.com');
    });

    it('deberia devolver mensaje vacio al iniciar', function() {
        let email = new Email();
        expect(email.getMensaje()).equal('');
    });
});
