var assert = require('assert');
var chai = require("chai");
var expect = require('chai').expect;
var should = require('chai').should();
var chaiAsPromised = require("chai-as-promised");

let TarjetaDeAsistencia = require('../../src/TarjetaDeAsistencia');
let Venta = require('../../src/TarjetaDeVenta');
let FechaTipoComision = require('../../src/Fecha de Pago Strategy/FechaTipoComision');
let FechaTipoFijo = require('../../src/Fecha de Pago Strategy/FechaTipoFijo');
let FechaTipoPorHora = require('../../src/Fecha de Pago Strategy/FechaTipoPorHora');
let SalarioComision = require('../../src/Salario Strategy/SalarioComision');
let SalarioFijo = require('../../src/Salario Strategy/SalarioFijo');
let SalarioPorHora = require('../../src/Salario Strategy/SalarioPorHora');
let EnCheque = require('../../src/Metodo de Pago Strategy/EnCheque');
let EnDeposito = require('../../src/Metodo de Pago Strategy/EnDeposito');
let EnEfectivo = require('../../src/Metodo de Pago Strategy/EnEfectivo');
let Email = require('../../src/Medios de Notificacion Composite/Email');
let Facebook = require('../../src/Medios de Notificacion Composite/Facebook');
let SMS = require('../../src/Medios de Notificacion Composite/SMS');
let WhatsApp = require('../../src/Medios de Notificacion Composite/WhatsApp');
let MediosDeNotificacion = require('../../src/Medios de Notificacion Composite/MediosDeNotificacion');
let PapeletaDePago = require('../../src/Papeleta de Pago/GeneradorPapeletasDePago');
let EmpleadoFijo = require('../../src/Database/Models/Empleado/EmpleadoFijo');
let EmpleadoPorComision = require('../../src/Database/Models/Empleado/EmpleadoPorComision');
let EmpleadoPorHora = require('../../src/Database/Models/Empleado/EmpleadoPorHora');
let Empleado = require('../../src/Empleado');

chai.use(chaiAsPromised);

describe('Test de empleado en la Base de datos', function() {

    it('deberia guardar un empleado tipo fijo en la base de datos', function() {
        let empleado;
        let asistencia = new TarjetaDeAsistencia("April 10, 2019 07:30 AM", "April 10, 2019 11:30 AM");
        let salario = new SalarioFijo(1000, asistencia);
        let fechaDePago = new FechaTipoFijo(salario);
        let metodoPago = new EnEfectivo();
        let papeletaDePago = new PapeletaDePago();
        papeletaDePago.agregarEmpleadoAPapeletasDePago(empleado);
        let mediosDeNotificacion = new MediosDeNotificacion(papeletaDePago);
        let whatsapp = new WhatsApp('79389772');
        mediosDeNotificacion.agregarMedioDeNotificacion(whatsapp);
        empleado = new Empleado("Mario", 76154235, fechaDePago, salario, metodoPago, mediosDeNotificacion, 79389772, "mario@gmail.com");
        return expect(EmpleadoFijo.persistirEmpleado(empleado)).to.eventually.equal(true); 
    });

    it('deberia guardar un empleado tipo por comision en la base de datos', function() {
        let empleado;
        let venta = new Venta();
        venta.addVenta('06-06-13', 100);
        let asistencia = new TarjetaDeAsistencia("April 10, 2019 07:30 AM", "April 10, 2019 11:30 AM");
        let salario = new SalarioComision(1000, 0.5, venta, asistencia);
        let fechaDePago = new FechaTipoComision(salario);
        let metodoPago = new EnDeposito();
        let papeletaDePago = new PapeletaDePago();
        papeletaDePago.agregarEmpleadoAPapeletasDePago(empleado);
        let mediosDeNotificacion = new MediosDeNotificacion(papeletaDePago);
        let whatsapp = new WhatsApp('79389772');
        let facebook = new Facebook("mario@gmail.com");
        mediosDeNotificacion.agregarMedioDeNotificacion(whatsapp);
        mediosDeNotificacion.agregarMedioDeNotificacion(facebook);
        empleado = new Empleado("Mario", 76154235, fechaDePago, salario, metodoPago, mediosDeNotificacion, 79389772, "mario@gmail.com");
        return expect(EmpleadoPorComision.persistirEmpleado(empleado)).to.eventually.equal(true); 
    });

    it('deberia guardar un empleado tipo por hora en la base de datos', function() {
        let empleado;
        let asistencia = new TarjetaDeAsistencia("April 10, 2019 07:30 AM", "April 10, 2019 11:30 AM");
        let salario = new SalarioPorHora(1000, asistencia);
        let fechaDePago = new FechaTipoPorHora(salario);
        let metodoPago = new EnCheque();
        let papeletaDePago = new PapeletaDePago();
        papeletaDePago.agregarEmpleadoAPapeletasDePago(empleado);
        let mediosDeNotificacion = new MediosDeNotificacion(papeletaDePago);
        let sms = new SMS('79389772');
        let email = new Email("mario@gmail.com");
        mediosDeNotificacion.agregarMedioDeNotificacion(sms);
        mediosDeNotificacion.agregarMedioDeNotificacion(email);
        empleado = new Empleado("Mario", 76154235, fechaDePago, salario, metodoPago, mediosDeNotificacion, 79389772, "mario@gmail.com");
        return expect(EmpleadoPorHora.persistirEmpleado(empleado)).to.eventually.equal(true); 
    });

    /*it('deberia devolver un empleado tipo fijo en la base de datos', function() {
        lista = [];
        let cuenta = new Cuenta("01","00","BNB","Sueldo","PagoEnCheque");
        let empleadoEjm = new Empleado("Mario", 8451717, 'fijo', 1000, null, cuenta, 7777777, "mario@gmail.com");
        lista.push(empleadoEjm);
        return expect(empleado.listaEmpleadosFijos()).to.eventually.deep.include(empleadoEjm)
    });

    it('deberia devolver un empleado tipo por comision en la base de datos', function() {
        let lista = [];
        let cuenta = new Cuenta("01","00","BNB","Sueldo","PagoEnDeposito");
        empleadoEjm = new Empleado("Marta", 8451717, 'comision', 1000, null, cuenta, 7777777, "marta@gmail.com");
        lista.push(empleadoEjm);
        return expect(empleado.listaEmpleadosPorComision()).to.eventually.deep.include(empleadoEjm)
    });

    it('deberia devolver un empleado tipo por hora en la base de datos', function() {
        let lista = [];
        let asistencia = new TarjetaDeAsistencia("Wed Apr 10 2019 07:30:00 GMT-0400", "Wed Apr 10 2019 11:30:00 GMT-0400");
        asistencia.addAsistencia(new Date("Wed Apr 10 2019 07:30:32 GMT-0400 (GMT-04:00)"));
        asistencia.addAsistencia(new Date("Thu Apr 11 2019 07:30:32 GMT-0400 (GMT-04:00)"));
        let cuenta = new Cuenta("01","00","BNB","Sueldo","PagoEnEfectivo");
        empleadoEjm = new Empleado("Marcos", 8451717, 'porHora', 100, asistencia, cuenta, 77777777, "marcos@gmail.com");
        lista.push(empleadoEjm);
        return expect(empleado.listaEmpleadosPorHora()).to.eventually.deep.include(empleadoEjm)
    });*/
});
