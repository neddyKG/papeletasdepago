var assert = require('assert');
var chai = require("chai");
var expect = require('chai').expect;
var should = require('chai').should();
var chaiAsPromised = require("chai-as-promised");

let papeletaDePagoDB = require('../../src/Database/Models/PapeletasDePago/PapeletaDePago');
let TarjetaDeAsistencia = require('../../src/TarjetaDeAsistencia');
let Venta = require('../../src/TarjetaDeVenta');
let FechaTipoComision = require('../../src/Fecha de Pago Strategy/FechaTipoComision');
let FechaTipoFijo = require('../../src/Fecha de Pago Strategy/FechaTipoFijo');
let FechaTipoPorHora = require('../../src/Fecha de Pago Strategy/FechaTipoPorHora');
let SalarioComision = require('../../src/Salario Strategy/SalarioComision');
let SalarioFijo = require('../../src/Salario Strategy/SalarioFijo');
let SalarioPorHora = require('../../src/Salario Strategy/SalarioPorHora');
let EnCheque = require('../../src/Metodo de Pago Strategy/EnCheque');
let EnDeposito = require('../../src/Metodo de Pago Strategy/EnDeposito');
let EnEfectivo = require('../../src/Metodo de Pago Strategy/EnEfectivo');
let Email = require('../../src/Medios de Notificacion Composite/Email');
let Facebook = require('../../src/Medios de Notificacion Composite/Facebook');
let SMS = require('../../src/Medios de Notificacion Composite/SMS');
let WhatsApp = require('../../src/Medios de Notificacion Composite/WhatsApp');
let MediosDeNotificacion = require('../../src/Medios de Notificacion Composite/MediosDeNotificacion');
let PapeletaDePago = require('../../src/Papeleta de Pago/GeneradorPapeletasDePago');
let EmpleadoFijo = require('../../src/Database/Models/Empleado/EmpleadoFijo');
let EmpleadoPorComision = require('../../src/Database/Models/Empleado/EmpleadoPorComision');
let EmpleadoPorHora = require('../../src/Database/Models/Empleado/EmpleadoPorHora');
let Empleado = require('../../src/Empleado');

chai.use(chaiAsPromised);

describe('Test de papeleta en la Base de datos', function() {

    it('deberia guardar una papeleta de un empleado fijo en la base de datos', function() {
        let empleado;
        let asistencia = new TarjetaDeAsistencia("April 10, 2019 07:30 AM", "April 10, 2019 11:30 AM");
        let salario = new SalarioFijo(1000, asistencia);
        let fechaDePago = new FechaTipoFijo(salario);
        let metodoPago = new EnEfectivo();
        let papeletaDePago = new PapeletaDePago();
        let mediosDeNotificacion = new MediosDeNotificacion(papeletaDePago);
        let whatsapp = new WhatsApp('79389772');
        mediosDeNotificacion.agregarMedioDeNotificacion(whatsapp);
        empleado = new Empleado("Mario", 76154235, fechaDePago, salario, metodoPago, mediosDeNotificacion, 79389772, "mario@gmail.com");
        papeletaDePago.agregarEmpleadoAPapeletasDePago(empleado);
        return expect(papeletaDePagoDB.persistirPapeletaDePago(papeletaDePago)).to.eventually.equal(true); 
    });

    it('deberia guardar una papeleta de un empleado tipo por comision en la base de datos', function() {
        let empleado;
        let venta = new Venta();
        venta.addVenta('06-06-13', 100);
        let asistencia = new TarjetaDeAsistencia("April 10, 2019 07:30 AM", "April 10, 2019 11:30 AM");
        let salario = new SalarioComision(1000, 0.5, venta, asistencia);
        let fechaDePago = new FechaTipoComision(salario);
        let metodoPago = new EnDeposito();
        let papeletaDePago = new PapeletaDePago();
        let mediosDeNotificacion = new MediosDeNotificacion(papeletaDePago);
        let whatsapp = new WhatsApp('79389772');
        let facebook = new Facebook("mario@gmail.com");
        mediosDeNotificacion.agregarMedioDeNotificacion(whatsapp);
        mediosDeNotificacion.agregarMedioDeNotificacion(facebook);
        empleado = new Empleado("Mario", 76154235, fechaDePago, salario, metodoPago, mediosDeNotificacion, 79389772, "mario@gmail.com");
        papeletaDePago.agregarEmpleadoAPapeletasDePago(empleado);
        return expect(papeletaDePagoDB.persistirPapeletaDePago(papeletaDePago)).to.eventually.equal(true); 
    });

    it('deberia guardar una papeleta de un empleado tipo por hora en la base de datos', function() {
        let empleado;
        let asistencia = new TarjetaDeAsistencia("April 10, 2019 07:30 AM", "April 10, 2019 11:30 AM");
        let salario = new SalarioPorHora(1000, asistencia);
        let fechaDePago = new FechaTipoPorHora(salario);
        let metodoPago = new EnCheque();
        let papeletaDePago = new PapeletaDePago();
        let mediosDeNotificacion = new MediosDeNotificacion(papeletaDePago);
        let sms = new SMS('79389772');
        let email = new Email("mario@gmail.com");
        mediosDeNotificacion.agregarMedioDeNotificacion(sms);
        mediosDeNotificacion.agregarMedioDeNotificacion(email);
        empleado = new Empleado("Mario", 76154235, fechaDePago, salario, metodoPago, mediosDeNotificacion, 79389772, "mario@gmail.com");
        papeletaDePago.agregarEmpleadoAPapeletasDePago(empleado);
        return expect(papeletaDePagoDB.persistirPapeletaDePago(papeletaDePago)).to.eventually.equal(true); 
    });

    /*it('deberia devolver una papeleta de un empleado tipo fijo en la base de datos', function() {
        return expect(papeletaDePago.listaPapeletaDePagos()).to.eventually.deep.include("Empleado: Mario Salario: 1000 Fecha de emision: 2019-4-22")
    });

    it('deberia devolver una papeleta de un empleado tipo por comision en la base de datos', function() {
        return expect(papeletaDePago.listaPapeletaDePagos()).to.eventually.deep.include("Empleado: Marta Salario: 1060 Fecha de emision: 2019-4-22")
    });

    it('deberia devolver una papeleta de un empleado tipo por hora en la base de datos', function() {
        return expect(papeletaDePago.listaPapeletaDePagos()).to.eventually.deep.include("Empleado: Marcos Salario: 400 Fecha de emision: 2019-4-22")
    });*/
});
